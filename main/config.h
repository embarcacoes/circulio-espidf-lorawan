#ifndef __CONFIG__H__
#define __CONFIG__H__

#include "sdkconfig.h"
#include <stdio.h>
#include <stdlib.h>

#define UART_NUM UART_NUM_1
#define UART_BAUD_RATE 9600
#define TXD_PIN (GPIO_NUM_16) // Pino de TX da UART
#define RXD_PIN (GPIO_NUM_17) // Pino de RX da UART
#define D9_PIN (GPIO_NUM_4)   // Pino do powerpin

#define WAIT_SEMAPHORE_TIME 4000
#define TX_INTERVAL 10000
#define LMIC_LOOP_TASK_PRIORITY 5
#define TX_TASK_PRIORITY 8

// LoRaWAN NwkSKey, network session key
static const unsigned char NWKSKEY[16] = {0};

// LoRaWAN AppSKey, application session key
static const unsigned char APPSKEY[16] = {0};
// LoRaWAN end-device address (DevAddr)
static const uint32_t DEVADDR =
    0x0000000; // <-- Change this address for every node!

#endif //!__CONFIG__H__