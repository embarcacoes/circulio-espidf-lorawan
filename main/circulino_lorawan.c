#include "config.h"
#include "driver/gpio.h"
#include "driver/uart.h"
#include "esp_event.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/projdefs.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "freertos/task.h"
#include "hal/gpio_types.h"
#include "types.h"
#include <lmic.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const char *TAG = "ESP_NODE";

const lmic_pinmap lmic_pins = {
    .nss = 18, // CS
    .rxtx = LMIC_UNUSED_PIN,
    .rst = 14,           // RESET
    .dio = {26, 35, 32}, // DIO 0, DIO 1, DIO 2
    .spi = {19, 27, 5},  // MISO, MOSI, CLK
};

/*
const lmic_pinmap lmic_pins = {
    .nss = 8, // CS
    .rxtx = LMIC_UNUSED_PIN,
    .rst = 4,         // RESET
    .dio = {3, 2, 1}, // DIO 0, DIO 1, DIO 2
    .spi = {5, 6, 7}, // MISO, MOSI, CLK
};
*/

SemaphoreHandle_t tx_semphore;

const int RX_BUF_SIZE = 1024; // Tamanho do buffer de leitura via UART
const int TX_BUF_SIZE = 25;   // Tamanho do buffer de escrita via UART
const int SEND_DATA_UART_QUEUE_SIZE =
    10; // Tamanho da fila de dados a serem enviados

QueueHandle_t send_data_uart_queue; // Fila de comunicação entre as tasks de

QueueHandle_t send_data_lora_queue;

void init_uart(void);
void remover_ponto(char *str);
void get_data(char *str, data_t *data);
int sendData(const char *logName, const char *data);
static void uart_tx_task(void *arg);
static void uart_rx_task(void *arg);
void init_sim808();
static void main_task(void *arg);
void onEvent(ev_t ev);
static void tx_send(unsigned char *dados);
void txTask(void *pvParameters);
void LMIC_Loop_Task(void *pvParameters);

void os_getArtEui(u1_t *buf) {}
void os_getDevEui(u1_t *buf) {}
void os_getDevKey(u1_t *buf) {}

void app_main() {

    ESP_LOGI(TAG, "Inicando Sistema");

    ESP_LOGI(TAG, "Criando semaforo");
    tx_semphore = xSemaphoreCreateBinary();

    os_init();

    vTaskDelay(pdMS_TO_TICKS(30));

    LMIC_reset();

    ESP_LOGI(TAG, "Configurando sessão LoRaWAN");
    LMIC_setSession(0x1, DEVADDR, (unsigned char *)NWKSKEY,
                    (unsigned char *)APPSKEY);

    ESP_LOGI(TAG, "Escolhendo banda");
    LMIC_selectSubBand(7);
    //  LMIC_setupChannel(0, 913500000, SF12, 7);
    //    Disable link check validation
    LMIC_setLinkCheckMode(0);

    // TTN uses SF9 for its RX2 window.
    LMIC.dn2Dr = DR_SF7;

    // Set data rate and transmit power for uplink (note: txpow seems to be
    // ignored by the library)
    ESP_LOGI(TAG, "Configurando TX Power");
    LMIC_setDrTxpow(DR_SF7, 30);

    ESP_LOGI(TAG, "Iniciando joining");

    LMIC_startJoining();

    send_data_lora_queue =
        xQueueCreate(SEND_DATA_UART_QUEUE_SIZE,
                     sizeof(data_t)); // Cria uma fila de envio de dados

    ESP_LOGI(TAG, "Criando Tasks");

    init_uart(); // Configura a comunicação via UART
    xTaskCreate(uart_rx_task, "uart_rx_task", 8192, NULL,
                configMAX_PRIORITIES - 1,
                NULL); // Cria a task de recepção de dados via UART
    xTaskCreate(uart_tx_task, "uart_tx_task", 4096, NULL,
                configMAX_PRIORITIES - 2,
                NULL); // Cria a task de envio de dados via UART
    xTaskCreate(main_task, "main_task", 4096, NULL, configMAX_PRIORITIES - 3,
                NULL); // Cria a task principal

    xTaskCreate(txTask, "TXTASK", 4096, NULL, TX_TASK_PRIORITY, NULL);
    xTaskCreatePinnedToCore(LMIC_Loop_Task, "LMIC_LOOP", 4096, NULL,
                            LMIC_LOOP_TASK_PRIORITY, NULL, 1);
}

static void tx_send(unsigned char *dados) {
    if (LMIC.opmode & OP_TXRXPEND) {
        ESP_LOGI(TAG, "OP_TXRXPEND, not sending");
    } else {
        // Prepare upstream data transmission at the next possible time.
        LMIC_setTxData2(1, dados, strlen((char *)dados), 0);
        ESP_LOGI(TAG, "Packet queued");
        LMIC_sendAlive();
    }
}

void remover_ponto(char *str) {
    int i, j = 0, len = strlen(str);
    for (i = 0; i < len; i++) {
        if (str[i] != '.') {
            str[j++] = str[i];
        }
    }
    str[j] = '\0';
}

void get_data(char *str, data_t *data) {
    int i = 0;

    char *token = strtok(str, ",");

    while (token != NULL) {

        token = strtok(NULL, ",");

        if (!token)
            break;

        printf("token: %s, i: %d\n", token, i);

        if (i == 0) {
            strlcpy(data->latitude, token, 12);
            remover_ponto(data->latitude);
        } else if (i == 1) {
            strlcpy(data->longitude, token, 12);
            remover_ponto(data->longitude);
        } else if (i == 2) {
            strlcpy(data->alt, token, 12);
            remover_ponto(data->alt);
            break;
        }
        i++;
    }
}

void txTask(void *pvParameters) {
    ESP_LOGI(TAG, "Iniciando Task de envio de dados");

    data_t buffer;
    unsigned char buffer_str[100];

    memset(buffer_str, 0, 100);

    xSemaphoreGive(tx_semphore);

    while (1) {
        if (xQueueReceive(
                send_data_lora_queue, &(buffer),
                portMAX_DELAY)) // Se tem algum dado na fila para ser enviado
        {

            if (xSemaphoreTake(tx_semphore,
                               pdMS_TO_TICKS(WAIT_SEMAPHORE_TIME)) == true) {
                sprintf((char *)buffer_str, "%s;%s;%.*s;00.00;C4",
                        buffer.longitude, buffer.latitude, 4, buffer.alt);

                ESP_LOGI(TAG, "Enviando dados: %s, tamanho: %d", buffer_str,
                         strlen((char *)buffer_str));

                tx_send(buffer_str);
            } else {
                ESP_LOGI(TAG, "Dados ainda não enviados, aguardando semaforo "
                              "ser liberado");

                vTaskDelay(pdMS_TO_TICKS(1000));
            }
        }
        vTaskDelay(pdMS_TO_TICKS(TX_INTERVAL));
    }

    vTaskDelete(NULL);
}

void LMIC_Loop_Task(void *pvParameters) {
    ESP_LOGI(TAG, "Iniciando Task de loop LMIC");

    while (1) {
        os_runloop_once();

        vTaskDelay(10);
    }

    vTaskDelete(NULL);
}

void onEvent(ev_t ev) {
    switch (ev) {
    case EV_SCAN_TIMEOUT:
        ESP_LOGI(TAG, "EV_SCAN_TIMEOUT");
        break;
    case EV_BEACON_FOUND:
        ESP_LOGI(TAG, "EV_BEACON_FOUND");
        break;
    case EV_BEACON_MISSED:
        ESP_LOGI(TAG, "EV_BEACON_MISSED");
        break;
    case EV_BEACON_TRACKED:
        ESP_LOGI(TAG, "EV_BEACON_TRACKED");
        break;
    case EV_JOINING:
        ESP_LOGI(TAG, "EV_JOINING");
        break;
    case EV_JOINED:
        ESP_LOGI(TAG, "EV_JOINED");
        break;
    case EV_RFU1:
        ESP_LOGI(TAG, "EV_RFU1");
        break;
    case EV_JOIN_FAILED:
        ESP_LOGI(TAG, "EV_JOIN_FAILED");
        break;
    case EV_REJOIN_FAILED:
        ESP_LOGI(TAG, "EV_REJOIN_FAILED");
        break;
    case EV_TXCOMPLETE:
        ESP_LOGI(TAG, "EV_TXCOMPLETE (includes waiting for RX windows)");
        if (LMIC.txrxFlags & TXRX_ACK)
            ESP_LOGI(TAG, "Received ack");
        if (LMIC.dataLen)
            ESP_LOGI(TAG, "Received %d bytes of payload", LMIC.dataLen);

        xSemaphoreGive(tx_semphore);
        break;
    case EV_LOST_TSYNC:
        ESP_LOGI(TAG, "EV_LOST_TSYNC");
        break;
    case EV_RESET:
        ESP_LOGI(TAG, "EV_RESET");
        break;
    case EV_RXCOMPLETE:
        // data received in ping slot
        ESP_LOGI(TAG, "EV_RXCOMPLETE");
        break;
    case EV_LINK_DEAD:
        ESP_LOGI(TAG, "EV_LINK_DEAD");
        break;
    case EV_LINK_ALIVE:
        ESP_LOGI(TAG, "EV_LINK_ALIVE");
        break;
    default:
        ESP_LOGI(TAG, "Unknown event");
        break;
    }
}

void init_uart(void) {
    /*
    Função responsavel por configurar e iniciar a UART
    */
    const uart_config_t uart_config = {
        // Struct com as confiugrações da UART
        .baud_rate = UART_BAUD_RATE,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .source_clk = UART_SCLK_DEFAULT,
        .rx_flow_ctrl_thresh = 122,
    };
    uart_driver_install(UART_NUM, RX_BUF_SIZE * 2, 0, 0, NULL,
                        0); // Escolhe a UART a ser utilizada
    uart_param_config(
        UART_NUM,
        &uart_config); // Configura a UART com base dos parametros do struct
    uart_set_pin(UART_NUM, TXD_PIN, RXD_PIN, UART_PIN_NO_CHANGE,
                 UART_PIN_NO_CHANGE); // Configura os pinos da UART

    send_data_uart_queue = xQueueCreate(
        SEND_DATA_UART_QUEUE_SIZE,
        sizeof(char) * TX_BUF_SIZE); // Cria uma fila de envio de dados
}

int sendData(const char *logName, const char *data) {
    /*
    Função responsavel por enviar dados via UART
    */
    const int len = strlen(data); // Obtenho o tamanho da string recebido
    const int txBytes =
        uart_write_bytes(UART_NUM_1, data, len); // Escreve os bytes na UART
    ESP_LOGI(logName, "Wrote %d bytes", txBytes);
    return txBytes; // Retorna a quantidade de bytes enviados
}

static void uart_tx_task(void *arg) {
    /*
    Task responsavel por realizar o envio dos dados via UART
    */
    static const char *TX_TASK_TAG =
        "TX_TASK"; // Crio uma tag de log especifica para os dados enviados
    esp_log_level_set(TX_TASK_TAG,
                      ESP_LOG_INFO); // Seto com uma cor de log diferente
    char rxBuffer[TX_BUF_SIZE];      // Aloco o buffer para enviar informações
    while (1)                        // Repito infinitamente
    {
        if (xQueueReceive(
                send_data_uart_queue, &(rxBuffer),
                portMAX_DELAY)) // Se tem algum dado na fila para ser enviado
        {
            sendData(TX_TASK_TAG, rxBuffer); // Envia os dados
        }
        vTaskDelay(50 / portTICK_PERIOD_MS); // Espera 50ms
    }
}

static void uart_rx_task(void *arg) {
    /*
    Task responsavel por ouvir e receber os dados da UART
    */
    data_t data_b;
    static const char *RX_TASK_TAG =
        "RX_TASK"; // Crio uma tag de log especifica para os dados recebidos
    esp_log_level_set(RX_TASK_TAG,
                      ESP_LOG_INFO); // Seto com uma cor de log diferente
    uint8_t *data = (uint8_t *)malloc(
        RX_BUF_SIZE + 1); // Aloco o buffer para receber informações
    while (1)             // Repito infinitamente
    {
        const int rxBytes = uart_read_bytes(
            UART_NUM_1, data, RX_BUF_SIZE,
            1000 / portTICK_PERIOD_MS); // Realiza a leitura do buffer da UART
        if (rxBytes > 0)                // Verifica se tem algo no buffer
        {
            data[rxBytes] =
                0; // Coloca a ultima posição do buffer após os dados como
                   // para sinalizar que ali e o fim da string
            ESP_LOGI(RX_TASK_TAG, "Read %d bytes: '%s'", rxBytes,
                     data); // Imprime os dados
            ESP_LOG_BUFFER_HEXDUMP(RX_TASK_TAG, data, rxBytes, ESP_LOG_INFO);

            if (strstr((const char *)data, "AT+CGPSINF=0")) {
                get_data((char *)data, &data_b);
                xQueueSend(send_data_lora_queue, &data_b, 1000);
            }
        }
    }
    free(data); // Desaloca o buffer
}

void init_sim808() {
    gpio_set_direction(D9_PIN, GPIO_MODE_OUTPUT);

    ESP_LOGI(TAG, "Iniciando SIM808");

    gpio_set_level(D9_PIN, 1);

    vTaskDelay(pdMS_TO_TICKS(1000));

    gpio_set_level(D9_PIN, 0);

    xQueueSend(send_data_uart_queue, "AT+CGNSPWR=1\n", 1000);

    vTaskDelay(pdMS_TO_TICKS(3000));

    ESP_LOGI(TAG, "SIM808 Iniciado");

    ESP_LOGI(TAG, "Ligando GPS");

    xQueueSend(send_data_uart_queue, "AT+CGPSPWR=1\n", 1000);

    vTaskDelay(pdMS_TO_TICKS(1000));

    xQueueSend(send_data_uart_queue, "AT+CGPSSTATUS?\n", 1000);

    vTaskDelay(pdMS_TO_TICKS(1000));
}

static void main_task(void *arg) {

    init_sim808();

    while (1) {
        xQueueSend(send_data_uart_queue, "AT+CGPSINF=0\n", 1000);

        vTaskDelay(pdMS_TO_TICKS(60000));
    }

    vTaskDelete(NULL);
}