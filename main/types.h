#ifndef __TYPES__H__
#define __TYPES__H__

typedef struct data_t {
    char longitude[12];
    char latitude[12];
    char alt[11];
    char velocidade[12];
    char pdop[4];
    char num_satelites[4];
} data_t;

#endif //!__TYPES__H__